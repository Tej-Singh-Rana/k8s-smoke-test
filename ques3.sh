#!/bin/bash

kubectl apply -f - <<EOF
apiVersion: v1
kind: Service
metadata:
  name: mysql-service
  namespace: beta
spec:
  ports:
  - port: 3306
    protocol: TCP
    targetPort: 3306
  selector:
    name: mysql
  type: ClusterIP

EOF


# To list the service

kubectl get svc,ep

